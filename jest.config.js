const config = {
  testEnvironment: 'jsdom',
  setupFiles: ['./test/jest.setup.js'],
};

export default config;
