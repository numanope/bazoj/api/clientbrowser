
# ApiClient Project

Part of bazoj.
The `ApiClient` project is a JavaScript library for making HTTP requests. It provides a simple and consistent API for interacting with API services.


## Features
- Supports any HTTP method.
- Checks network status before querying.


## Installation
You can install the `ApiClient` library using npm:

```bash
npm install apiclient
```


## Usage

Here's a basic example of how to use the `ApiClient`:

```javascript
import ApiClient from 'apiclient';

const client = new ApiClient();
client.set('GET', 'https://api.example.com/data');
client.query()
  .then(data => console.log(data));
```


## Contributing

We welcome contributions!


## License
Please refer to the [LICENSE](./LICENSE.md) file.
