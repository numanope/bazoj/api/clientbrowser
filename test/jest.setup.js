global.window = Object.create(window);

Object.defineProperty(window, 'fetch', {
  value: jest.fn().mockImplementation(() => Promise.resolve({
    json: () => Promise.resolve({ key: 'value' }),
  })),
});

Object.defineProperty(window, 'navigator', {
  value: {
    onLine: true,
  },
});
