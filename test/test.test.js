import ApiClient from '../index.js';

describe('ApiClient', () => {
  let client;

  beforeEach(() => {
    client = new ApiClient();
  });

  test('isOnline returns true', () => {
    expect(client.isOnline()).toBe(true);
  });

  test('set assigns values correctly', () => {
    client.set('type', 'GET', 'https://example.com', [['Content-Type', 'application/json']], { key: 'value' });
    expect(client.type).toBe('type');
    expect(client.method).toBe('GET');
    expect(client.url).toBe('https://example.com');
    expect(client.headers).toEqual([['Content-Type', 'application/json']]);
    expect(client.body).toEqual({ key: 'value' });
  });

  test('setType assigns type correctly', () => {
    client.setType('type');
    expect(client.type).toBe('type');
  });

  test('setMethod assigns method correctly', () => {
    client.setMethod('GET');
    expect(client.method).toBe('GET');
  });

  test('setUrl assigns url correctly', () => {
    client.setUrl('https://example.com');
    expect(client.url).toBe('https://example.com');
  });

  test('setHeaders assigns headers correctly', () => {
    client.setHeaders([['Content-Type', 'application/json']]);
    expect(client.headers).toEqual([['Content-Type', 'application/json']]);
  });

  test('setBody assigns body correctly', () => {
    client.setBody({ key: 'value' });
    expect(client.body).toEqual({ key: 'value' });
  });

  test('query makes a request with correct parameters', async () => {
    client.set('type', 'GET', 'https://example.com', [['Content-Type', 'application/json']], { key: 'value' });
    const data = await client.query();
    expect(window.fetch).toHaveBeenCalledWith('https://example.com', {
      method: 'GET',
      headers: [['Content-Type', 'application/json']],
    });
    expect(data).toEqual({ key: 'value' });
  });

  test('query throws an error when offline', async () => {
    window.navigator.onLine = false;
    client.set('type', 'GET', 'https://example.com');
    await expect(client.query()).rejects.toThrow('Offline');
  });

  test('query throws an error becasue of incorrect parameters', async () => {
    client.set('type', 'GET', 'not a url', [['Content-Type', 'application/json']], { key: 'value' });
    await expect(client.query()).rejects.toThrow();
  });  
});
